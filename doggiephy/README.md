## Doggiephy

## Project setup
**to initialize the project**
```

npm install

```
### Compiles and hot-reloads for development

```

npm run serve

```



### Compiles and minifies for production

```

npm run build

```



### Lints and fixes files

```

npm run lint

```
To start the project you need an .env file with the variables:

    VUE_APP_BASE_URL=
    VUE_APP_API_KEY=


## Summary

This project consumes the **[giphy api](https://developers.giphy.com/branch/master/docs/api/)**. It only does the search for dog gifs. Any word will be added to the word 'dog'.


The project contains three sections:

**Home
History
Favorites**

In the **Home** section a search for the gif desired by the user is performed. By default, it displays a list of random gif's, once the search is performed it displays a list of gifs related to the user's search.

The **History** view shows a list of each search performed by the user, while searching for a gif.

**Favorites** shows the gifs that have been marked as favorites by the user.



## Implementation

Inside the **api.js** file, two methods were created that are in charge of fetching all the gifts from giphy and searching with respect to the user's search.

    export  const  getAllGifs  =  async () => {

    try {

    const  require  =  await  fetch(`${process.env.VUE_APP_BASE_URL}?q=dogs&api_key=${process.env.VUE_APP_API_KEY}&limit=36`)

    const  apiResponse  =  await  require.json()

    return  await  apiResponse.data

    } catch (getAllGifsError) {

    console.error(getAllGifsError)

    }

    }


    export  const  searchGifs  =  async (searchWords) => {

    try {

    const  require  =  await  fetch(`${process.env.VUE_APP_BASE_URL}?q=dogs-${searchWords}&api_key=${process.env.VUE_APP_API_KEY}&limit=36`)

    const  apiResponse  =  await  require.json()

    return  await  apiResponse.data

    } catch (searchGifsError) {

    console.error(searchGifsError)

    }

    }


Inside the **Home.vue** file there is an input responsible for sending a request and then rendering the response in a list.

The styles are created with tailwind.css and provide the mobile-first.

All searches are stored in the local-storage, to be later rendered in the **Searches.vue** file.
All searches are stored in the local-storage, to be later rendered in the Searches.vue file.

inside Searches.vue there is a method called created, in charge of bringing all the searches saved in local-storage.

    created() {

    let  data  =  localStorage.getItem("searches");

    if (data  !=  null) {

    this.mySearches =  JSON.parse(data);

    }

    },

Los gifs marcados como favoritos, se guardan en localstorage con la función **saveFavoriteGif** dentro del componente **Card.vue**

    saveFavoriteGif(myFavoriteGif) {

    this.toggle();

    if (localStorage.getItem("favorites") ==  null) {

    localStorage.setItem("favorites", "[]");

    }

    let  oldData  =  JSON.parse(localStorage.getItem("favorites"));

    oldData.push(myFavoriteGif);



    localStorage.setItem("favorites", JSON.stringify(oldData));

    },



## Done criteria

[x] Responsive design
[x] Components
[x] Animations
[x] Save searches in localstorage
[x] Save favorites in localstorage

