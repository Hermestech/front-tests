import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Home'
import Searches from '@/views/Searches'
import Favorites from '@/views/Favorites'

Vue.use(Router)

export const router = new Router ({
    mode:'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/history',
            name: 'search',
            component: Searches
        },
        {
            path: '/favorites',
            name: 'favorites',
            component: Favorites
        },
    ]
})