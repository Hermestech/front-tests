export const getAllGifs = async () => {
    try {
        const require = await fetch(`${process.env.VUE_APP_BASE_URL}?q=dogs&api_key=${process.env.VUE_APP_API_KEY}&limit=36`)
        const apiResponse = await require.json()
        return await apiResponse.data
    } catch (getAllGifsError) {
        console.error(getAllGifsError)
    }
}

export const searchGifs = async (searchWords) => {
    try {
        const require = await fetch(`${process.env.VUE_APP_BASE_URL}?q=dogs-${searchWords}&api_key=${process.env.VUE_APP_API_KEY}&limit=36`)
        const apiResponse = await require.json()
        return await apiResponse.data
    } catch (searchGifsError) {
        console.error(searchGifsError)
    }
}