import Vue from "vue";
import Router from 'vue-router'
import Home from '@/views/Home'
import CountryDetail from '@/views/CountryDetail'
import WorldCup from '@/views/WorldCup'

Vue.use(Router)

export const router = new Router({
    mode:'history',

    routes: [
            {
                path: '/',
                name: 'home',
                component: Home
            },
            {
                path: '/country/:id',
                name: 'country-team',
                component: CountryDetail
            },
            {
                path: '/worldcup',
                name: 'world-cup',
                component: WorldCup
            }
    ]
})