export const getAllCountries = async () => {
    try {
        const require = await fetch(`${process.env.VUE_APP_BASE_URL}soccer/countries?apikey=${process.env.VUE_APP_API_KEY}`)
        const apiResponse = await require.json()
        return await apiResponse.data
    } catch (error) {
        console.error(error)
    }
}

export const getTeamsByCountryId = async (countryId) => {
    try {
        const require = await fetch(`${process.env.VUE_APP_BASE_URL}soccer/teams?apikey=${process.env.VUE_APP_API_KEY}&country_id=${countryId}`)
        const apiResponse = await require.json()
        return await apiResponse.data
    } catch (error) {
        console.error(error)
    }
}

export const getAllSeasons = async (leagueId) => {
    try {
        const require = await fetch(`${process.env.VUE_APP_BASE_URL}soccer/seasons?apikey=${process.env.VUE_APP_API_KEY}&league_id=${leagueId}`)
        const apiResponse = await require.json()
        return await apiResponse.data
    } catch (error) {
        console.error(error)
    }
}

export const getMatches = async (seasonId,dateFrom) => {
    try {
        const require = await fetch(`${process.env.VUE_APP_BASE_URL}soccer/matches?apikey=${process.env.VUE_APP_API_KEY}&season_id=${seasonId}&date_from=${dateFrom}`)
        const apiResponse = await require.json()
        return await apiResponse.data
    } catch (error) {
      console.error(error)
    }
}